# privacy
data collection is not cool.

## Nothing more than needed is kept
All logs are disabled
All services are hosted using Docker. The logging driver has been set to "none" in docker to disable logs. Nginx logs are also sent to /dev/null. Here are the configs:

Docker:
```json
{
    "log-driver": "none",
    "runtimes": {
        "nvidia": {
            "args": [],
            "path": "nvidia-container-runtime"
        }
    }
}
```

Nginx:
```
...

	access_log /dev/null;
	error_log /dev/null;
...
```
### Databases
Databases are used for services like Invidious and Piped. These of course store any data you give to them, and nothing more.

## Nothing is shared to anybody outside of drgns.space
There's really no reason for them to have any data on drgns.space, so it won't be shared.
Backups are kept on-site, on the same server as drgns.space, with the only access being from me.




